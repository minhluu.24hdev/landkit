// assets function
function $(query) {
    return document.querySelector(query)
}

function $$(query) {
    return document.querySelectorAll(query)
}

function convertTimestampToString(timestamp) {
    const date = new Date(timestamp * 1000); // convert seconds to milliseconds
    const dateString = date.toLocaleDateString(); // convert to localized date string
    return dateString;
}

// pagination
let employees = []
let curPage = 1;
let perPage = $('.selection').value
let totalPage
let perEmployees = []
let searchArr



// re-render and fetch data func
function reRenderDataWithPagination() {
    if (searchArr !== undefined) {
        totalPage = searchArr.length / perPage
        if (!Number.isInteger(totalPage)) {
            totalPage = Number.parseInt(totalPage) + 1
        }
        perPage = Number.parseInt(perPage)
        perEmployees = searchArr.slice(
            (curPage - 1) * perPage,
            (curPage - 1) * perPage + perPage
        )
    } else {
        totalPage = employees.length / perPage
        if (!Number.isInteger(totalPage)) {
            totalPage = Number.parseInt(totalPage) + 1
        }
        perPage = Number.parseInt(perPage)
        perEmployees = employees.slice(
            (curPage - 1) * perPage,
            (curPage - 1) * perPage + perPage
        )
    }
    renderPageNumber()
    const data = renderData(perEmployees)
    mountedElm(data)
}

async function fetchData() {
    const response = await fetch(`https://63c6a3164ebaa802854a2ebe.mockapi.io/api/lnmtodo/employee`)
    const data = await response.json()
    employees = data;
    totalPage = employees.length / perPage
    if (!Number.isInteger(totalPage)) {
        totalPage = Number.parseInt(totalPage) + 1
    }
    perEmployees = employees.slice(
        (curPage - 1) * perPage,
        (curPage - 1) * perPage + perPage
    )
    return data
}

// handle event func
function handleChangePageNumber(num) {
    if (num !== curPage) {
        curPage = num
        reRenderDataWithPagination()
    }
}

function handleChangeDropList(e) {
    const select = $('.selection')
    const newPerPage = select.options[select.selectedIndex].value
    console.log({ newPerPage, perPage });
    perPage = Number.parseInt(newPerPage)
    curPage = 1
    reRenderDataWithPagination()

}

//sort
function handleSortEvent(e) {
    let domClasses = e.target.classList
    let classes = [...domClasses]
    const property = classes[3].toLowerCase()
    const isSorted = classes.filter(elm => {
        return elm === 'asc' || elm === 'desc'
    })
    if (isSorted.length > 0) {
        if (isSorted[0] === 'asc') {
            e.target.classList.remove('asc')
            e.target.classList.add('desc')
            sortDesc(property)
        } else if (isSorted[0] === 'desc') {
            e.target.classList.remove('desc')
            e.target.classList.add('asc')
            sortAsc(property)
        }
    } else {
        removeSortClass()
        e.target.classList.add('asc')
        sortAsc(property)
    }
}

function removeSortClass() {
    const sortElms = $$('.title')
    sortElms.forEach((elm) => {
        elm.classList.remove('asc')
        elm.classList.remove('desc')
    })
}

function sortDesc(property) {
    if (searchArr === undefined) {
        let sortEmployees = employees.sort((a, b) => {
            if (a[property] < b[property]) {
                return 1
            }
            if (a[property] > b[property]) {
                return -1
            }
            return 0
        })
        employees = sortEmployees
    } else {
        const sortData = searchArr.sort((a, b) => {
            if (a[property] < b[property]) {
                return 1
            }
            if (a[property] > b[property]) {
                return -1
            }
            return 0
        })
        searchArr = sortData
    }
    reRenderDataWithPagination()
}

function sortAsc(property) {
    if (searchArr === undefined) {
        let sortEmployees = employees.sort((a, b) => {
            if (a[property] < b[property]) {
                return -1
            }
            if (a[property] > b[property]) {
                return 1
            }
            return 0
        })
        employees = sortEmployees


    } else {
        const sortData = searchArr.sort((a, b) => {
            if (a[property] < b[property]) {
                return -1
            }
            if (a[property] > b[property]) {
                return 1
            }
            return 0
        })
        searchArr = sortData
    }
    reRenderDataWithPagination()
}

// Render
function renderHeader(data) {
    let dataKeys = Object.keys(data[0])
    dataKeys = dataKeys.filter(elm => elm !== "id")
    dataKeys = dataKeys.map(elm => {
        return elm[0].toLocaleUpperCase() + elm.slice(1)
    })
    const trElm = document.createElement('tr')
    trElm.classList.add('w-full')
    const htmlTag = dataKeys.map(data => {
        return `<th class='text-left px-2 title ${data}'>
        ${data}        
        </th>`
    }).join('')
    trElm.innerHTML = htmlTag

    return trElm
}

function renderData(data) {
    const htmlTags = data.map(elm => {
        let newData = ''
        for (const [key, value] of Object.entries(elm)) {
            if (key !== 'id') {
                if (key === 'startDate') {
                    newData += `<td class='text-left'>${convertTimestampToString(value)}</td>`

                } else
                    newData += `<td class='text-left'>${value}</td>`
            }
        }
        return `<tr>${newData}</tr>`
    }).join('')
    return htmlTags
}

function renderPageNumber() {
    const paginationWrap = $('.pagination-wrap')
    paginationWrap.innerHTML = ''
    if (!Number.isInteger(totalPage)) {
        totalPage = Number.parseInt(totalPage) + 1
    }
    for (let i = 1; i <= totalPage; i++) {
        paginationWrap.innerHTML +=
            `<li 
            class='btn ${i === curPage ? 'primary-page' : ""}'
            onClick='handleChangePageNumber(${i})
            '>
                ${i}
            </li>`
    }
}

function mountedElm(elm) {
    const tableTag = $('.main-table')
    // console.log({ tableTag });
    if (typeof elm === "string") {

        tableTag.querySelector(".t-body").innerHTML = elm
    } else {
        // header
        tableTag.querySelector(".t-head").appendChild(elm)
    }
}


// Main
async function main() {
    await fetchData()
    // render Header
    const headerElms = renderHeader(employees)
    mountedElm(headerElms)
    // Render data
    const dataElms = renderData(perEmployees)
    mountedElm(dataElms)
    renderPageNumber()

    // Drop down list event 
    const dropDownList = $('.selection')
    dropDownList.addEventListener('change', handleChangeDropList)


    // previous btn and next btn
    const prevBtn = $('.prev')
    prevBtn.addEventListener('click', () => {
        if (curPage === 1) {

        } else {
            handleChangePageNumber(curPage - 1)
        }
    })
    const nextBtn = $('.next')
    nextBtn.addEventListener('click', () => {
        if (curPage === totalPage) {

        } else {
            handleChangePageNumber(curPage + 1)
        }
    })


    // search
    const searchInput = $('.search')
    searchInput.addEventListener('change', (e) => {
        console.log(e.target.value);
        const searchData = employees.filter(employee => {
            return employee.name.includes(e.target.value)
        })
        searchArr = searchData
        curPage = 1
        reRenderDataWithPagination()
    })

    // sort event
    const sortElms = $$('.title')
    sortElms.forEach((elm) => {
        elm.addEventListener("click", handleSortEvent)
        elm.addEventListener("click", handleSortEvent)
    })

}

main()