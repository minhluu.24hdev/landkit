const animationText = document.querySelector('.animation-text')
const textLoad = () => {
    setTimeout(() => {
        animationText.innerText = "Developer. ";
    }, 0);
    setTimeout(() => {
        animationText.innerText = "Founder. ";
    }, 2500);
    setTimeout(() => {
        animationText.innerText = "Designer. ";
    }, 5000);
}

textLoad()
setInterval(textLoad, 7500)