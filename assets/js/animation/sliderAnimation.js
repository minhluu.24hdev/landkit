const sliderBtn = document.querySelectorAll(".slider-btn")
const imgList = document.querySelectorAll(".slider-img")
const textWrapElm = document.querySelector(".slider-text-change")
const htmlElm1 = `
<div class='slider-text-change'>
    <img src="./assets/img/svgexport-21.svg" alt="" class="slider-text__logo">
    <p class="slider-text-desc">
        “I've never used a theme as versatile and flexible as Landkit. It's my go to for
        building landing sites on almost any project.”
    </p>
    <div class="slider-text-author">Russ D'Sa</div>
</div>

`
const htmlElm2 = `
<div class='slider-text-change'>
    <img src="./assets/img/svgexport-20.svg" alt="" class="slider-text__logo">
    <p class="slider-text-desc">
    “Landkit is hands down the most useful front end Bootstrap theme I've ever used. I can't wait to use it again for my next project.”
    </p>
    <div class="slider-text-author">Dave Gamache</div>
</div>

`
let isDisplay = false;

const handleSlideImg = async (index) => {
    const container = document.querySelector('.slider-text-container')
    const primary = document.querySelector('.primary')
    const backup = document.querySelector('.backup')
    const sliders = document.querySelectorAll('.slider-text-container')
    console.log(backup.offsetLeft);
    if (isDisplay) {
        imgList[1].classList.remove("appearImg")
        imgList[1].classList.add("hideImg")
    } else {
        imgList[1].classList.remove("hideImg")
        imgList[1].classList.add("appearImg")
    }

    // prev
    if (index === 0) {
        if (backup.classList.contains('left')) {
            backup.style = 'transform: translateX(0%);'
            primary.style = 'opacity: 0'
            await setTimeout(() => {
                primary.style = 'opacity: 0; transform: translateX(-100%);'
                primary.style.opacity = 1
            }, 200)
            backup.classList.remove('backup')
            backup.classList.remove('left')
            backup.classList.add('primary')
            primary.classList.remove('primary')
            primary.classList.add('backup')
            primary.classList.add('left')
        } else {
            backup.style = 'transform: translateX(-100%); transition-duration: 0.0001s'
            primary.style = 'opacity: 0'
            await setTimeout(() => {
                primary.style = 'opacity: 0; transform: translateX(-100%);'
                primary.style.opacity = 1
            }, 200)
            await setTimeout(() => {
                backup.style = 'transform: translateX(0%);'
            }, 100)
            backup.classList.remove('backup')
            backup.classList.remove('right')
            backup.classList.add('primary')
            primary.classList.remove('primary')
            primary.classList.add('backup')
            primary.classList.add('left')
        }
    }
    // next
    else if (index === 1) {
        if (backup.classList.contains('right')) {
            backup.style = 'transform: translateX(0%);'
            primary.style = 'opacity: 0'
            await setTimeout(() => {
                primary.style = 'opacity: 0; transform: translateX(100%);'
                primary.style.opacity = 1
            }, 200)
            backup.classList.remove('backup')
            backup.classList.remove('right')
            backup.classList.add('primary')
            primary.classList.remove('primary')
            primary.classList.add('backup')
            primary.classList.add('right')
        } else {
            backup.style = 'transform: translateX(100%); transition-duration: 0.0001s'
            primary.style = 'opacity: 0'
            await setTimeout(() => {
                primary.style = 'opacity: 0; transform: translateX(100%);'
                primary.style.opacity = 1
            }, 200)
            await setTimeout(() => {
                backup.style = 'transform: translateX(0%);'
            }, 100)
            backup.classList.remove('backup')
            backup.classList.remove('left')
            backup.classList.add('primary')
            primary.classList.remove('primary')
            primary.classList.add('backup')
            primary.classList.add('right')
        }
    }
    isDisplay = !isDisplay

}

sliderBtn.forEach((btn, index) => {
    btn.addEventListener('click', () => {
        handleSlideImg(index)
    })
})


