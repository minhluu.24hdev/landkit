
const numbers = document.querySelectorAll(".number")

const handleIncreaseNUmber = () => {
    const elmHeight = numbers[0].getBoundingClientRect().top;
    const triggerBottom = window.innerHeight / 5 * 4
    if (elmHeight < triggerBottom) {
        numbers.forEach((elm, idx) => {
            switch (idx) {
                case 0:
                    let startNum = 0
                    let timerId
                    timerId = setInterval(() => {
                        if (startNum < 100) {
                            startNum += 1;
                            elm.textContent = `${startNum}%`
                        } else {
                            clearInterval(timerId)
                        }
                    }, 7)
                    break
                case 1:
                    let startNum2 = 0
                    let startHour = 0
                    let timerId2
                    timerId2 = setInterval(() => {
                        if (startNum2 < 24 || startHour < 7) {
                            startNum2 += 1;
                            startHour = startHour === 7 ? startHour : startHour + 1;
                            elm.textContent = `${startNum2}/${startHour}`
                        } else {
                            clearInterval(timerId2)
                        }
                    }, 60)
                    break
                case 2:
                    let startNumber = 0
                    let timerId3
                    timerId3 = setInterval(() => {
                        if (startNumber < 100) {
                            startNumber += 1;
                            elm.textContent = `${startNumber}k+`
                        } else {
                            clearInterval(timerId3)
                        }
                    }, 7)
                    setTimeout(() => {
                        window.removeEventListener("scroll", handleIncreaseNUmber)
                    }, 300)
                    break
                default:
                    console.log("hehe")
            }

        })
    }
}

window.addEventListener("scroll", handleIncreaseNUmber)

