function handleRevealAnimation(elm, animationClass) {
    const elmHeight = elm.getBoundingClientRect().top;
    const triggerBottom = window.innerHeight / 5 * 4
    if (elmHeight < triggerBottom) {
        elm.classList.add(animationClass)
    }
}


function main() {
    const swipeLeftAnimation = 'swipe-left-animation'
    const swipeRightAnimation = 'swipe-right-animation'
    const swipeUpAnimation1 = 'swipe-up-animation-1'
    const swipeUpAnimation2 = 'swipe-up-animation-2'
    const swipeUpAnimation3 = 'swipe-up-animation-3'

    const swipeLeftElms = document.querySelectorAll('.swipe-left')
    swipeLeftElms.forEach(elm => {
        handleRevealAnimation(elm, swipeLeftAnimation)
    })

    const swipeRightElms = document.querySelectorAll('.swipe-right')
    swipeRightElms.forEach(elm => {
        handleRevealAnimation(elm, swipeRightAnimation)
    })

    const swipeDelay1 = document.querySelectorAll('.swipe-1')
    swipeDelay1.forEach(elm => {
        handleRevealAnimation(elm, swipeUpAnimation1)
    })

    const swipeDelay2 = document.querySelectorAll('.swipe-2')
    swipeDelay2.forEach(elm => {
        handleRevealAnimation(elm, swipeUpAnimation2)
    })

    const swipeDelay3 = document.querySelectorAll('.swipe-3')
    swipeDelay3.forEach(elm => {
        handleRevealAnimation(elm, swipeUpAnimation3)
    })

}
main()
window.addEventListener('scroll', main)