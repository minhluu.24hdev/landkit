const switchElm = document.querySelector('.switch');
switchElm.addEventListener('click', () => {
    const checkboxPrice = document.querySelector('.price-checkbox')
    const moneyElm = document.querySelector('.money')
    if (checkboxPrice.checked) {
        let startMoney = 29
        let timerId
        timerId = setInterval(() => {
            if (startMoney < 49) {
                startMoney += 1;
                moneyElm.textContent = `${startMoney}`
            } else {
                clearInterval(timerId)
            }
        }, 20)
    } else {
        let startMoney = 49
        let timerId
        timerId = setInterval(() => {
            if (startMoney > 29) {
                startMoney -= 1;
                moneyElm.textContent = `${startMoney}`
            } else {
                clearInterval(timerId)
            }
        }, 20)
    }
})