function handleOpenMenu(e) {
    const isTrigger = e.target.closest('.mobile-menu')
    if (document.querySelector('.checkbox-modal').checked && isTrigger) {
        console.log("open");
        document.body.style.overflowY = 'hidden'
        document.querySelector('.modal-menu').classList.add('showModal')
    }
}

function handleCloseMenu(e) {
    const isTrigger = e.target.closest('.navbar-exit')
    if (isTrigger) {
        console.log("close");
        document.body.style.overflowY = 'scroll'
        document.querySelector('.modal-menu').classList.remove('showModal')
        document.querySelector('.checkbox-modal').checked = false;
    }


}


function main() {
    const openMenuBtn = document.querySelector(".mobile-menu")
    const closeMenuBtn = document.querySelector(".navbar-exit")
    console.log({ openMenuBtn, closeMenuBtn });
    openMenuBtn.addEventListener('click', handleOpenMenu)
    closeMenuBtn.addEventListener('click', handleCloseMenu)
}
main()

