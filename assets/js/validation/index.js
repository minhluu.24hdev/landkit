function Validator(formCLass) {
    const formSelector = document.querySelector(formCLass)
    let isValid = true;
    const validatorRules = {
        required: function (value) {
            return (value && value.trim().length > 0) ? undefined : "Please enter this input"
        },
        email: function (value) {
            const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
            return regex.test(value) ? undefined : "Please enter a valid email"
        },
        min: function (min) {
            return function (value) {
                return value.trim().length > min ? undefined : `Please enter a password with length > ${min}`
            }
        }
    }
    const formRules = {}
    if (formSelector) {
        const inputs = formSelector.querySelectorAll("[name][rules]")
        inputs.forEach(input => {
            let rules = input.getAttribute('rules').split('|')
            rules = rules.map(rule => {
                return rule.includes(":") ? [...rule.split(":")] : rule
            })

            rules.forEach(rule => {
                if (Array.isArray(rule)) {
                    const ruleFunc = validatorRules[rule[0]]
                    formRules[input.name] = (formRules[input.name]) ?
                        [...formRules[input.name], ruleFunc(rule[1])] :
                        [ruleFunc(rule[1])]
                } else {
                    formRules[input.name] = (formRules[input.name]) ?
                        [...formRules[input.name], validatorRules[rule]] :
                        [validatorRules[rule]]
                }
            })
            input.onkeypress = handleValidateInputWithEnter
        })
        function handleValidateInput(e) {
            const rules = formRules[e.target.name]
            let errMsg;
            rules.find(rule => {
                errMsg = rule(e.target.value)
                return errMsg
            })
            console.log(errMsg);
            if (errMsg) {
                isValid = false
                document.querySelector(`.${e.target.name}-label`).textContent = errMsg
            } else {
                isValid = true
                document.querySelector(`.${e.target.name}-label`).textContent = ''
            }
        }

        function handleValidateInputWithEnter(e) {
            if (e.key === "Enter") {
                const rules = formRules[e.target.name]
                let errMsg;
                rules.find(rule => {
                    errMsg = rule(e.target.value)
                    return errMsg
                })
                console.log(errMsg);
                if (errMsg) {
                    isValid = false
                    document.querySelector(`.${e.target.name}-label`).textContent = errMsg
                } else {
                    isValid = true
                    document.querySelector(`.${e.target.name}-label`).textContent = ''

                }
            }
        }
        // console.log({ formRules });
        formSelector.addEventListener("submit", (e) => {
            e.preventDefault()
            const result = {}
            const inputs = formSelector.querySelectorAll("[name][rules]")
            inputs.forEach(input => {
                handleValidateInput({
                    target: input
                })
                result[input.name] = input.value
            })
            if (isValid) {
                console.log(result)
            }
        })
    }

}
